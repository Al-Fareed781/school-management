import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Student> studList1 = new ArrayList<Student>();
         studList1.add(new Student("Alwyn"));
         studList1.add(new Student("Joseph"));
         studList1.add(new Student("Sinchana"));
         studList1.add(new Student("Jose"));

         List<Subject> subList1 = new ArrayList<>();
         subList1.add(new Subject("English"));
         subList1.add(new Subject("Maths"));

         List<Teacher> teacherLsit = new ArrayList<>();
         teacherLsit.add(new Teacher("Sinthya", studList1, subList1));
         teacherLsit.add(new Teacher("Shwetha", studList1, subList1));
         teacherLsit.add(new Teacher("Fathima", studList1, subList1));

        System.out.println("Enter the name of the Teacher : ");
            String teacherName = sc.next();

         System.out.println("Name of the Teacher : "+teacherName);
        boolean found=false;
       
         for (Teacher teacher : teacherLsit) {
            if(teacher.getTeacher().equals(teacherName)){
                teacher.displayStudents();
                teacher.displaySubjects();
                found = true;
                return;
            }
         }
         if(!found){
            System.out.println("Not found..!");
         }
         sc.close();
    }
}

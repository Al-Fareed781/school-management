import java.util.*;

public class Teacher {
    private String TName;
    private List<Student> studList = new ArrayList<>(0);
    private List<Subject> subList = new ArrayList<>(0);

    public Teacher(String TName, List<Student> stud, List<Subject> sub) {
        this.TName = TName;
        this.studList = stud;
        this.subList = sub;
    }

    public String getTeacher() {
        return TName;
    }

    public List<Student> getStudents() {
        return studList;
    }

    public List<Subject> getSubjects() {
        return subList;
    }

    public void displayStudents() {
        System.out.println("Students : ");

        for (Student student : studList) {
            System.out.println(student);
        }
    }

    public void displaySubjects() {
        System.out.println("Subjects : ");

        for (Subject sub : subList) {
            System.out.println(sub);
        }
    }
}
